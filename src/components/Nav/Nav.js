import React, { Component } from 'react';
import { Route, NavLink, Switch } from 'react-router-dom';
import './Nav.css';
import Settings from '../Settings/Settings';
import GeneralNotificationsList from '../GeneralNotifications/GeneralNotificationsList';
import DispatchNotificationsList from '../DispatchNotifications/DispatchNotificationsList';

class Nav extends Component {
    render() {
        return (
            <header className="row">
                <nav className="Nav col-5">
                    <ul>
                        <li className="extraPadding"><NavLink to="/" exact activeClassName="active" >Settings</NavLink></li>
                        <li className="extraPadding"><NavLink activeClassName="active" to="/generalnotifications">General Notifications and Alarms</NavLink></li>
                        <li className="extraPadding"><NavLink activeClassName="active" to="/dispatchnotifications">Dispatch Notifications</NavLink></li>
                    </ul> 
                    <div className="text-center">  
                        <Switch>
                            <Route
                                path="/"
                                exact
                                component={() => <Settings/>} />
                            <Route
                                path="/generalnotifications"
                                exact
                                component={() => <GeneralNotificationsList />} />
                            <Route
                                path="/dispatchnotifications"
                                exact
                                component={() => <DispatchNotificationsList />} />
                        </Switch>
                    </div>
                </nav>
            </header>
        )
    }

}

export default Nav;
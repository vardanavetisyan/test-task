import React from 'react';
import Button from '../../Button/Button'

class InputAll extends React.Component {
    constructor() {
      super();
      this.state = {
        phoneValidation: '+',
        shareholders: [{ phoneValidation: '+' }],
        emailValidation: '',
        shareholders2: [{emailValidation: ''}]
      };
    }
    
    phoneValidation = (evt) => {
      this.setState({ phoneValidation: evt.target.value });
    }

    emailValidation = (evt) => {
      this.setState({ emailValidation: evt.target.value });
    }
    
    handleShareholderNameChange = (idx) => (evt) => {
        this.setState({ 
            phoneValidation: evt.target.value,
              
      });
      const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
        if (idx !== sidx) return shareholder;
        return { ...shareholder, phoneValidation: evt.target.value };
      });
      
      this.setState({ 
          shareholders: newShareholders,
    });
    }

    handleShareholderNameChange2 = (idx) => (evt) => {
        this.setState({ 
            emailValidation: evt.target.value,
      });
      const newShareholders2 = this.state.shareholders2.map((shareholder2, sidx) => {
        if (idx !== sidx) return shareholder2;
        return { ...shareholder2, emailValidation: evt.target.value };
      });
      
      this.setState({ shareholders2: newShareholders2 });
    }
    
    
    handleAddShareholder = () => {
      this.setState({ shareholders: this.state.shareholders.concat([{ phoneValidation: '' }]) });
    }
    handleAddShareholder2 = () => {
      this.setState({ shareholders2: this.state.shareholders2.concat([{ emailValidation: '' }]) });
    }
    
    handleRemoveShareholder = (idx) => () => {
      this.setState({ shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx) });
    }
    handleRemoveShareholder2 = (idx) => () => {
      this.setState({ shareholders2: this.state.shareholders2.filter((s, sidx) => idx !== sidx) });
    }
    
    render() {
      return (
          <div className="row">
          <div className="container">
          <form className="form-inline text-center"> 
              <div className="col-6">
                {this.state.shareholders.map((shareholder, idx) => (
                    <div key={idx} className="shareholder">
                    <input
                        type="tel"
                        placeholder="Tel"
                        value={shareholder.phoneValidation}
                        onChange={this.handleShareholderNameChange(idx)}
                    />
                    <button type="button" onClick={this.handleRemoveShareholder(idx)} className="small">-</button>
            </div>
          ))}
          <button type="button" onClick={this.handleAddShareholder} className="small">Add Shareholder</button>
          </div>

              <div className="col-6">
                {this.state.shareholders2.map((shareholder2, idx) => (
                    <div key={idx} className="shareholder">
                    <input
                        type="email"
                        placeholder="example@example.com"
                        value={shareholder2.emailValidation}
                        onChange={this.handleShareholderNameChange2(idx)}
                    />
                    <button type="button" onClick={this.handleRemoveShareholder2(idx)} className="small">-</button>
                    </div>
          ))}
          <button type="button" onClick={this.handleAddShareholder2} className="small">Add Shareholder</button>
          </div>
          <Button phoneValidation={this.state.phoneValidation} emailValidation={this.state.emailValidation}/>
        </form>
        </div>
        </div>
      )
    }
  }
  
export default InputAll
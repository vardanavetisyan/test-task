import React from 'react';
import GeneralNotifications from './GeneralNotifications';

const generalNotifications = (props) => {
    const generalNotifications = [
        'Plan trial expire',
        'Account free trial expire tomorrow',
        'Account plan expire',
        'Account plan expire tomorrow',
        'Dispatches low'
    ];

    const notificationsList = generalNotifications.map((item,index) => (<GeneralNotifications key={index} notificationType={item}/>))
    return (
       <div>
        {notificationsList}
       </div>
      
    )
}

export default generalNotifications;
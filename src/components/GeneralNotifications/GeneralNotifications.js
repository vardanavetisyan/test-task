import React from 'react';
import ToggleButton from '../Button/toggleButton'
import './General.css'

const generalNotifications = (props) => {
    return(
        <div className="generalField">
           <span className="sliCing">{props.notificationType}</span> <ToggleButton />
           <hr/>
        </div>
    )
}
export default generalNotifications
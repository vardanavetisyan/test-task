import React from 'react';

const button = (props) => {
   let { emailValidation } = props

   const emailValid = (emailValidation) => {
       const emailRegex = /^([A-Za-z0-9_\-.+])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,})$/;
       return emailRegex.test(emailValidation);
   }
   return (
       <button
           disabled={props.phoneValidation.length === 12 &&
               props.phoneValidation[0] === '+' &&
               emailValid(emailValidation) ? null : true}
           type='submit' style={{ backgroundColor: 'white' }}>Save</button>
   )
}

export default button;
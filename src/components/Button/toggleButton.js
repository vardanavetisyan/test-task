import React from 'react';
import './ToggleButton.css';

const toggleButton = () => {
    return (
        <label className="switch">
            <input type="checkbox"  />
            <span className="slider round"></span>
        </label>
    )

}

export default toggleButton
import React from 'react';
import ToggleButton from '../Button/toggleButton'
import './Dispatch.css'

const dispatchNotificationsItem = (props) => {
    return(
        <div className="dispatchField">
        
           <span className="sliCing">{props.notificationType}</span>
           
           <span className="addPadding"><ToggleButton /></span>
           {props.id === 1 ||
             props.id === 2 ||
              props.id === 5 ||
               props.id === 9 ||
               props.id === 10 ||
               props.id === 17 ||
               props.id === 33 ? <span className="addPadding"><ToggleButton /></span> : null}
               <hr/>
        </div>
    )
}
export default dispatchNotificationsItem
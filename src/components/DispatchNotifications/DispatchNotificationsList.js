import React from 'react';
import DispatchNotifications from './DispatchNotifications'
const dispatchNotifications = (props) => {
    const dispatchNotifications = [
        'Dispatch created',
        'Quick dispatch request',
        'Dispatch accepted',
        'Dispatch didnt selected',
        'Dispatch expired',
        'Offer accepted',
        'Offer declined',
        'Offer expired',
        'Dispatch not confirmed',
        'Dispatch canceled',
        'Dispatch updated',
        'Dispatch scheduled',
        'Dispatch marked as picked up',
        'Dispatch pick up appears to be late',
        'Checklist for dispatch required',
        'Bol for dispatch was signed on pick up',
        'Dispatch scheduled for delivery in hours',
        'Delivery for dispatch appear to be late',
        'Dispatch delivered',
        'Bol for dispatch was signed as delivered',
        'Invoice sent',
        'Invoice edited',
        'Invoice unpaid remainder',
        'Invoice adjustment request',
        'Invoice paid',
        'Dispatch paid',
        'Payment declined for dispatch',
        'Feedback for dispatch',
        'Rate user for dispatch',
        'Rate user for dispatch edited',
        'Rate user for dispatch deleted',
        'Rate for dispatch edited',
        'Rate for dispatch deleted',
        'Insurance is set to expire'
    ];

    const dispatchNotificationsList = dispatchNotifications.map((item,index) => (<DispatchNotifications 
     key={index} id={index} notificationType={item}/> ))
    return (
       <div>
        {dispatchNotificationsList}
       </div>
      
    )
}

export default dispatchNotifications;
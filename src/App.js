import React, { Component } from 'react';
import './App.css';
import Nav from './components/Nav/Nav';
import { BrowserRouter } from 'react-router-dom'


class App extends Component {
  render() {
    return (
      <div className="row"> 
        <div className="container">
          <div className="largeWidth">
            <BrowserRouter>
              <div className="container-fluid">
                <Nav />
              </div>
            </BrowserRouter>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
